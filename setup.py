import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ai4eo-forecast",
    version="0.0.1",
    author="Gábor Péterffy",
    author_email="peterffy95@gmail.com",
    description="Open source library to compare Earth Observation and weather forecast services with the actual measurements and assess the accuracy of the forescast",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/DeepBlueGlobe/ai4eo-forecast",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Development Status :: 1 - Planning",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Information Analysis",
    ],
    install_requires=[
          'dask',
          'xarray',
          'numpy',
          'Cython',
          'shapely',
          'cartopy',
      ],
)
