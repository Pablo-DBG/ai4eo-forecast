.. AI4EO forecast documentation master file, created by
   sphinx-quickstart on Sun Jun 23 13:57:36 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AI4EO forecast's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


This will be an open source library to compare Earth Observation and weather forecast services with the actual measurements and assess the accuracy of the forescast. The documentation of the different modules and their member's can be found below.

data_fetcher module
-------------------
.. automodule:: ai4eo_forecast.data_fetcher
    :members:

utility module
--------------
.. automodule:: ai4eo_forecast.utility
    :members:

visualization module
--------------------
.. automodule:: ai4eo_forecast.visualization
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
