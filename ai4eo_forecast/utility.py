import os
import sys
import xarray as xr
import numpy as np


def find_nc_files_in_folder(path):
    r"""
    Search for files with nc extension in the given path.

    Parameters
    ----------
    path : `str`
        Path to the folder containing the *.nc files

    Returns
    -------
    `list()`
        Alphabetically ordered list of *.nc files in the given folder
    """
    files = []
    # Iteration over files at the given path and checking for nc extension
    for i in os.listdir(path):
        if i.endswith(".nc"):
            files.append(os.path.join(path, i))
    # The results are sorted in alphabetical order
    files.sort()
    return files


def load_data_into_list(files):
    r"""
    Opens the datasets and stores them in a list.

    Parameters
    ----------
    files : `list()`
        List of paths to datasets.

    Returns
    -------
    `list()`
        List of xarray datasets containing the datasets in order as in the
        provided files list.
    """
    data = []
    for i in files:
        data.append(xr.open_dataset(i, chunks={"time": "auto"}))
    return data


def calculate_msqe_between_datasets(data,
                                    reference,
                                    mean_along_axis=None,
                                    data_filename=None,
                                    reference_filename=None):
    r"""
    Calculates the mean square error for the given two datasets.

    Parameters
    ----------
    data : `xarray.DataArray()` or `list()`
        The dataset or list of datasets to be compared
    reference : `xarray.DataArray()`
        The dataset which is the reference during the comparison. It must be
        compatible in dimension with the other dataset.
    mean_along_axis : `tuple()` or `list()`
        The dimensions for what the summation during the calculation should be
        done.
    data_filename : `str` or `list()`
        This will be stored as an attribute in the result with filename keyword
        if set
    reference_filename : `str`
        This will be stored as an attribute in the result with compared_to
        keyword if set

    Returns
    -------
    `xarray.DataArray()` or `list()`
        Dataset containing the mean square error values or list of these if
        the data was a list as well.
    """
    if isinstance(data, list):
        if data_filename is not None and len(data_filename) != len(data):
            error = ("data list dimension %d and filename list "
                     "dimension %d does not match!")
            raise RuntimeError(error % (len(data), len(data_filename)))
        _data = data
        _data_filename = data_filename
    else:
        # Create list from the simple instances so no code duplication is
        # neeeded for the actual calculation
        _data = [data, ]
        if data_filename is not None:
            _data_filename = [data_filename, ]
        else:
            _data_filename = None

    result = []
    for i, j in enumerate(_data):
        # Calculation
        diffsq = np.square(j-reference)
        e = diffsq.mean(dim=mean_along_axis, skipna=True)

        # Storing additional info
        e.attrs = dict(name="Mean square error")
        if _data_filename is not None:
            e.attrs.update({"filename": _data_filename[i]})
        if reference_filename is not None:
            e.attrs.update({"compared_to": reference_filename})
        result.append(e)
    if isinstance(data, list):
        return result
    elif len(result) == 1:
        return result[0]
    else:
        raise RuntimeError("Bad parameters, should not reach")


class CompatibilityReport:
    r"""
    Contains details about the relation of two datasets.

    Parameters
    ----------

    md : `dict()`
        Dictionary of dimension names where the key is the name of the
        dimension in one dataset and the value is the name in the other.
    dc : `dict()`
        Dictionary, where the name is the name of the dimension in the
        studied dataset while the value is True if the two dataset is
        compatible in that dimension, otherwise it is false.
    cr : `dict(dict())`
        Dictionary, where the key is the name of the dimension and the value
        is an other dictionary containing the `min` and `max` keys which
        represent the common range between the two dataset. There is no entry
        for dimensions where there is no overlap.


    Attributes
    ----------

    mutual_dimensions : `dict()`
        See `md` above.
    dimension_compatibility : `dict()`
        See `dc` above.
    common_range : `dict()`
        See `cr` above.
    """

    def __init__(self, md=None, dc=None, cr=None):
        if md is None:
            self.mutual_dimensions = {}
        else:
            self.mutual_dimensions = md
        if dc is None:
            self.dimension_compatibility = {}
        else:
            self.dimension_compatibility = dc
        if cr is None:
            self.common_range = {}
        else:
            self.common_range = cr

    def __repr__(self):
        return ("CompatibilityReport(md=%r, dc=%r, cr=%r)"
                "") % (self.mutual_dimensions,
                       self.dimension_compatibility,
                       self.common_range)

    def __str__(self):
        return ("CompatibilityReport(md=%s, dc=%s, cr=%s)"
                "") % (self.mutual_dimensions,
                       self.dimension_compatibility,
                       self.common_range)

    def show(self):
        counter = 0
        sys.stdout.write("# Compatible dimensions\n")
        for i, j in self.dimension_compatibility.items():
            if j:
                sys.stdout.write(("{}, {}, mutual range: {} {}\n"
                                  "").format(i,
                                             self.mutual_dimensions[i],
                                             self.common_range[i]["min"],
                                             self.common_range[i]["max"]))
            else:
                counter += 1
        if counter > 0:
            sys.stdout.write("# Incompatible dimensions\n")
            for i, j in self.dimension_compatibility.items():
                if not j:
                    sys.stdout.write("{}, {}\n".format(i,
                                     self.mutual_dimensions[i]))


def compatibility_check(reference_dataset, dataset, dictionary=None):
    r"""
    Compares the given dataset to the reference and returns with a
    `CompatibilityReport`.

    Parameters
    ----------

    reference_dataset : `xarray.Dataset`
        This dataset will be used as a reference point for comparison.
    dataset : `xarray.Dataset`
        This dataset will be compared to the `reference_dataset`.
    dictionary : `dict()`
        If it is None, the comparison will consider that the different
        dimensions have the same name in the two datasets. Otherwise a
        dictionary should be specified where the key is the name of the
        dimension in the dataset and the value is the name in the reference
        dataset.
    """
    # Which dimension corresponds to which in the two dataset
    report = CompatibilityReport()
    _dict = {}
    if dictionary is None:
        for i in dataset.dims:
            # Store only those elements where a name connection is available
            if i in reference_dataset.dims:
                _dict.update({i: i})
    else:
        for i, j in dictionary.items():
            if i in dataset.dims and j in reference_dataset.dims:
                _dict.update({i: j})
    report.mutual_dimensions = _dict
    for i, j in _dict.items():
        if not np.array_equal(dataset[i].data, reference_dataset[j].data):
            d_min = dataset[i].min().data
            d_max = dataset[i].max().data
            rd_min = reference_dataset[j].min().data
            rd_max = reference_dataset[j].max().data
            collected = {}
            if d_min <= rd_min and d_max >= rd_min:
                collected.update({"min": rd_min})
            elif d_min >= rd_min and d_min <= rd_max:
                collected.update({"min": d_min})

            if d_max <= rd_max and d_max >= rd_min:
                collected.update({"max": d_max})
            elif d_max >= rd_max and d_min <= rd_max:
                collected.update({"max": rd_max})

            if len(collected) == 0:
                report.dimension_compatibility.update({i: False})
            else:
                report.dimension_compatibility.update({i: True})
                report.common_range.update({i: collected})
        else:
            ran = {"min": dataset[i].min().data,
                   "max": dataset[i].max().data}
            report.common_range.update({i: ran})
            report.dimension_compatibility.update({i: True})
    return report
