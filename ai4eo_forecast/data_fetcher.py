import argparse
import sys
import ftplib
import sqlite3
import time
import os
from ftplib import FTP
from ftplib import all_errors as ftp_error
from datetime import datetime


class DataFetcher:
    r"""
    This class creates a local copy of the data what can be found on the given
    server.

    It is able to determine the change of the latest modification time
    of a given file and then a download is initiated to have a local copy as
    well. The timestamp is appended to the files but the directory structure is
    preserved.

    Parameters
    ----------
    cmd : `bool`
        If cmd is not None, argument parsing will be done on the arguments
        provided to the program, other parameters may be ignored.
    address : `str`
        Address of the ftp server where from the data should be retrieved.
        Note: Ignored if cmd is not None.
    paths : `list()`
        List or tuple containing folder paths on the server what should be
        searched through recursively.
        Note: ignored if cmd is not None.
    user : `str`
        Username for the ftp server.
        Note: ignored if cmd is not None and the corresponding argument is
        set for the run.
    password : `str`
        Password of the given user on the ftp server.
        Note: ignored if cmd is not None and the corresponding argument is
        set for the run.
    storage : `str`
        Path where the database of files and the copies of the files should
        be stored. The default is the current working directory.
        Note: ignored if cmd is not None.
    now : `bool`
        If it is True, files created in the past will be not downloaded.
        Note: ignored if cmd is not None.
    loop : `bool`
        If it is True an infinite loop will be started with a timeout
        between each fetch loop.
        Note: ignored if cmd is not None.
    timeout : `int`
        Waiting time in seconds before restarting the fetch cycle.
        Note: ignored if cmd is not None.
    verbose : `bool`
        Default is True. Prints status messages during run.
    """

    def __init__(self,
                 cmd=None,
                 address=None,
                 paths=None,
                 user=None,
                 password=None,
                 storage=".",
                 now=False,
                 loop=False,
                 timeout=3600,
                 verbose=True):
        r"""
        Init the DataFetcher class.

        Parameters
        ----------
        cmd : `bool`
            If cmd is not None, argument parsing will be done on the arguments
            provided to the program, other parameters may be ignored.
        address : `str`
            Address of the ftp server where from the data should be retrieved.
            Note: Ignored if cmd is not None.
        paths : `list()`
            List or tuple containing folder paths on the server what should be
            searched through recursively.
            Note: ignored if cmd is not None.
        user : `str`
            Username for the ftp server.
            Note: ignored if cmd is not None and the corresponding argument is
            set for the run.
        password : `str`
            Password of the given user on the ftp server.
            Note: ignored if cmd is not None and the corresponding argument is
            set for the run.
        storage : `str`
            Path where the database of files and the copies of the files should
            be stored. The default is the current working directory.
            Note: ignored if cmd is not None.
        now : `bool`
            If it is True, files created in the past will be not downloaded.
            Note: ignored if cmd is not None.
        loop : `bool`
            If it is True an infinite loop will be started with a timeout
            between each fetch loop.
            Note: ignored if cmd is not None.
        timeout : `int`
            Waiting time in seconds before restarting the fetch cycle.
            Note: ignored if cmd is not None.
        verbose : `bool`
            Default is True. Prints status messages during run.
        """
        self.connection = None
        self.verbose = verbose
        if (cmd is not None) and (cmd is True):
            # Parsing parameters
            parser = argparse.ArgumentParser(description=("Downloading "
                                                          "continously updated"
                                                          " datasets from FTP "
                                                          "servers."))
            parser.add_argument("address",
                                metavar="SERVER",
                                type=str,
                                nargs=1,
                                help=("Ftp address of the server (without the "
                                      "protocol prefix)."))
            parser.add_argument("paths",
                                metavar="PATH",
                                type=str,
                                nargs="+",
                                help=("Paths which should be monitored "))
            parser.add_argument("--user",
                                help="Username on the server",
                                type=str,
                                nargs=1)
            parser.add_argument("--password",
                                help="Password for the user",
                                type=str,
                                nargs=1)
            parser.add_argument("--storage",
                                help=("An empty/previous project folder for "
                                      "the same address (should exists)"),
                                type=str,
                                nargs=1,
                                default=".")
            parser.add_argument("--now",
                                action='store_true')
            parser.add_argument("--loop",
                                action='store_true')
            parser.add_argument("--timeout",
                                help=("Period between two successful check of "
                                      "the server"),
                                type=int,
                                nargs=1,
                                default=3600)

            args = parser.parse_args()

            # Store data for later use and query unknown data
            self.server_address = args.address[0].lstrip("ftp://").rstrip("/")
            self.paths = args.paths

            self.user = None
            if args.user is not None:
                self.user = args.user[0]
            elif user:
                self.user = user

            self.password = None
            if args.password is not None:
                self.password = args.password[0]
            elif password:
                self.password = password

            if self.verbose:
                sys.stdout.write(("The following paths are going to be "
                                  "scanned for data:\n"))
                for i in self.paths:
                    sys.stdout.write("ftp://%s/%s\n" % (self.server_address,
                                                        i))

            self.storage = args.storage[0].rstrip("/")
            self.db_path = self.storage + "/" + "database.db"

            self.time_limit = (args.now, datetime.now())
            self.timeout = args.timeout
            self.loop = args.loop
        else:
            self.server_address = address.lstrip("ftp://").rstrip("/")
            self.paths = paths
            self.user = user
            self.password = password
            self.storage = storage.rstrip("/")
            self.db_path = self.storage + "/" + "database.db"
            self.time_limit = (now, datetime.now())
            self.timeout = timeout
            self.loop = loop

    def connect(self):
        r"""
        Connect to the local database. If not called by user,
        fetch() will invoke it.
        """
        try:
            self.connection = sqlite3.connect(self.db_path)
            c = self.connection.cursor()
            # Checking for tables. Create them if neccessary
            c.execute("""
                      CREATE TABLE IF NOT EXISTS paths(
                      id integer PRIMARY KEY,
                      path text NOT NULL,
                      folder boolean
                      );
                      """)
            c.execute("""
                      CREATE TABLE IF NOT EXISTS files(
                      id integer PRIMARY KEY,
                      pid integer NOT NULL,
                      last_update text NOT NULL,
                      downloaded boolean,
                      FOREIGN KEY (pid) REFERENCES paths (id)
                      );
                      """)
        except sqlite3.Error as e:
            print(e)
            raise

    def fetch(self):
        r"""
        Fetch data from the server. If loop is set it will behave
        accordingly. Calls connect() if the local database is not present.

        Raises
        ------
        `ftplib.all_errors`
            In case of ftp error
        `sqlite3.Error`
            In case of database error
        `OSError`
            In case of data handling on the local machine caused an error.
        """
        if self.connection is None:
            self.connect()

        while True:
            # Status messages
            if self.verbose:
                sys.stdout.write("Fetch cycle started...\n")
                sys.stdout.write(("Initiating connection with the server"
                                  "...\n"))

            # Connect to the server
            try:
                ftp = FTP(host=self.server_address,
                          user=self.user,
                          passwd=self.password)
            except ftp_error as e:
                sys.stderr.write("Connection failed.")
                print(e)
                raise

            # Status message
            if self.verbose:
                sys.stdout.write("Connected\n")

            # Iterate over the given paths and their subdirectories
            for i in self.paths:
                # Status message
                if self.verbose:
                    sys.stdout.write("Fetching %s...\n" % i)
                self.recursive_lookup(ftp, "/" + i.lstrip("/"))
            try:
                c = self.connection.cursor()
                c.execute(("SELECT pid,last_update,id FROM "
                          "files WHERE downloaded=?;"), (False,))
                rows = c.fetchall()
                if self.verbose:
                    sys.stdout.write(("Number of missing "
                                      "files: %d\n") % len(rows))
                # Download starts
                if len(rows) > 0:
                    if self.verbose:
                        sys.stdout.write("Download starts...")
                    for i in rows:
                        c.execute(("SELECT path FROM "
                                  "paths WHERE id=?;"), (i[0],))
                        rows2 = c.fetchall()
                        if len(rows2) != 1:
                            raise RuntimeError("Unexpected database error")
                        # Create local folder structure
                        path = rows2[0][0].rstrip("/")
                        path = path.lstrip("/")
                        folder = path[0:path.rfind("/")]
                        folder = folder.rstrip("/")
                        if folder != "":
                            local_folder = (self.storage.rstrip("/") +
                                            "/" + folder)
                            try:
                                os.makedirs(local_folder, exist_ok=True)
                            except OSError as e:
                                print(e)
                                raise
                            # Modify the filename to contain the last
                            # modification in the filename
                            local_path = (self.storage.rstrip("/") +
                                          "/" + path)
                            extension = local_path[local_path.rfind("."):]
                            local_path = local_path.rstrip(extension)
                            local_path = (local_path + ("_%s" % i[1]) +
                                          extension)
                            try:
                                with open(local_path, "wb") as binary:
                                    ftp.retrbinary("RETR %s" % rows2[0],
                                                   binary.write)
                                c.execute(("UPDATE files SET downloaded = ? "
                                           "WHERE id = ?;"), (True, i[2]))
                            except ftp_error as e:
                                sys.stderr.write(("Error while downloading %s."
                                                  " Skipping...\n") %
                                                 rows2[0][0])
                                print(e)
                    if self.verbose:
                        sys.stdout.write("Download finished.")
            except sqlite3.Error as e:
                print(e)
                raise
            # Store the accumulated data locally
            self.connection.commit()
            if self.verbose:
                sys.stdout.write("Fetch cycle finished.\n")
            if self.loop:
                time.sleep(self.timeout)
            else:
                break

    def insert_new_directory_into_db(self, path):
        r"""
        Inserts directory with the given path into the database if
        it does not exist in it already.

        Parameters
        ----------
        path : `str`
            Path of the directory.

        Raises
        ------
        `sqlite3.Error`
            In case of database error.
        """
        try:
            c = self.connection.cursor()
            c.execute("SELECT * FROM paths WHERE path=?;", (path,))
            rows = c.fetchall()
            if len(rows) > 0:
                return
            else:
                c.execute(("INSERT INTO paths "
                          "(path, folder) VALUES(?,?);"), (path, True))
        except sqlite3.Error as e:
            print(e)
            raise

    def insert_file_into_db(self, path):
        r"""
        Inserts the file with the given path into the database if
        it does not exist already.

        Note: It does not create a local entry, update_file_in_db()
        has to be invoked after this call.

        Parameters
        ----------
        path : `str`
            Server path of the given file

        Raises
        ------
        `sqlite3.Error`
            In case of database error.
        """
        try:
            c = self.connection.cursor()
            c.execute("SELECT * FROM paths WHERE path=?;", (path,))
            rows = c.fetchall()
            if len(rows) == 0:
                c.execute(("INSERT INTO paths (path, folder) "
                           "VALUES(?,?);"), (path, False))
        except sqlite3.Error as e:
            print(e)
            raise

    def check_if_directory(self, ftp, path):
        r"""
        Checks if the given path is a file or a directory. First it
        tries to lookup it in the local databse, and if there are no
        results it checks it on the ftp server.

        Parameters
        ----------
        ftp : `FTP()`
            Alive ftp connection to the server.
        path : `str`
            Path to the file/directory in question.

        Returns
        -------
        `(bool, bool)`
            The first one is True if the given path corresponds to a
            directory and the second one is True if it is already
            in the database.

        Raises
        ------
        `sqlite3.Error`
            In case of database related error happens.
        `ftplib.all_errors`
            In case of ftp related unhandled exception happens.
        """

        folder = False
        in_db = False

        # Check for data about the given path in the database first
        try:
            c = self.connection.cursor()
            c.execute(("SELECT folder FROM paths "
                       "WHERE path=?;"), (path,))
            rows = c.fetchall()
            if len(rows) > 1:
                raise RuntimeError(("Corrupted database! Multiple "
                                   "entries with the same path!"))
            elif len(rows) == 1:
                in_db = True
                if rows[0][0] == 1:
                    folder = True
        except sqlite3.Error as e:
            print(e)
            raise

        # There was no result in the local database
        if not in_db:
            # Check if it is a directory or a file on the ftp server
            try:
                ftp.cwd(path)
                folder = True
            except ftp_error as e:
                if str(e) == "550 Failed to change directory.":
                    pass
                else:
                    raise
        return folder, in_db

    def check_if_file_is_in_db(self, path):
        r"""
        Checks if the file with given path is already in the databse
        or not.

        Parameters
        ----------
        path : `str`
            Path of the file in question.

        Returns
        -------
        `bool`
            True if the file with the given path is already in the
            database, False otherwise.

        Raises
        ------
        `sqlite3.Error`
            In case of database error.
        """
        try:
            c = self.connection.cursor()
            c.execute("SELECT * FROM paths WHERE path=?;", (path,))
            rows = c.fetchall()
            if len(rows) == 0:
                return False
            else:
                return True
        except sqlite3.Error as e:
            print(e)
            raise

    def update_file_in_db(self, ftp, path):
        r"""
        Check if the last modification time of the given file matches
        with any of the entries already in the database. If not,
        stores it and marks it for download.

        Parameters
        ----------
        ftp : `FTP()`
            Alive ftp connection to the server.
        path : `str`
            Path of the file to be checked.

        Raises
        ------
        `sqlite3.Error`
            In case of the database related error
        `RuntimeError`
            In case of the database is corrupt
        """
        try:
            c = self.connection.cursor()
            # Select the file from the db
            c.execute("SELECT id FROM paths WHERE path=?;", (path,))
            rows = c.fetchall()
            if len(rows) != 1:
                raise RuntimeError(("Database error during entry "
                                    "update! %s is not, or more than "
                                    "once in the table") % path)
            pid = rows[0][0]
            # Checking for entries for the given file
            c.execute("SELECT last_update FROM files WHERE pid=?;",
                      (pid,))
            rows = c.fetchall()

            # Check modification for the current file
            server_mdtm = ftp.sendcmd("MDTM " + path).split(" ")[1]
            c.execute(("SELECT * FROM files WHERE pid=? "
                      "AND last_update=?;"), (pid, server_mdtm))
            rows = c.fetchall()
            if len(rows) == 0:
                dt = datetime.strptime(server_mdtm, "%Y%m%d%H%M%S")
                if self.time_limit[0] and dt < self.time_limit[1]:
                    return
                c.execute(("INSERT INTO files (pid, last_update, "
                           "downloaded) VALUES (?,?,?);"),
                          (pid, server_mdtm, False))
        except sqlite3.Error as e:
            print(e)
            raise
        except ftp_error as e:
            sys.stderr.write(("Error while requesting of MDMT "
                              "of %s, skipping...\n") % (path))
            print(e)

    def recursive_lookup(self, ftp, cwd):
        r"""
        A recursive file lookup and download based on the current database
        and the provided connection and path details.

        Parameters
        ----------
        ftp : `FTP()`
            A working FTP connection.
        cwd : `str`
            The root folder of the recursion.

        Raises
        ------
        `ftplib.all_errors`
            In case of ftp error
        """
        try:
            folder, in_db = self.check_if_directory(ftp, cwd)
            if folder:
                if not in_db:
                    self.insert_new_directory_into_db(cwd)
                ftp.cwd(cwd)
                try:
                    files = ftp.nlst()
                    for i in files:
                        self.recursive_lookup(ftp, cwd + "/" + i)
                except ftplib.error_perm as resp:
                    # https://stackoverflow.com/questions/111954/
                    # using-pythons-ftplib-to-get-a-directory-listing-portably
                    if str(resp) == "550 No files found":
                        pass
                    else:
                        raise
            else:
                in_db = self.check_if_file_is_in_db(cwd)
                if in_db:
                    self.update_file_in_db(ftp, cwd)
                else:
                    self.insert_file_into_db(cwd)
                    self.update_file_in_db(ftp, cwd)
        except ftp_error as e:
            sys.stderr.write("Error while accessing %s\n" % cwd)
            print(e)
            raise
