import numpy as np
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.animation import FuncAnimation


def plot_geographic_data(data,
                         figsize=(10, 10),
                         projection=None,
                         longitude="longitude",
                         latitude="latitude",
                         levels=100,
                         filename=None,
                         show=True,
                         locator=None
                         ):
    r"""
    Plots geographic data with a filled contourplot.

    Parameters
    ----------
        data : `xarray.DataArray`
            The dataset which should be plotted.
        figsize : `tuple()`
            The size of the figure.
        projection : `Projection`
            A projection to be used for the geographic data. Default is
            `cartopy.crs.PlateCarree()`
        longitude : `str`
            Name of the longitude dimension in the dataset
        latitude : `str`
            Name of the latitude dimension in the dataset
        levels : `int`
            Number of levels to be used
        filename : `str`
            Saves the figure into this file if set
        show : `bool`
            Shows the figure if true
        locator :
            If set it will be provided to matplotlib contourf
    """
    # If projection is not set, use the default
    if projection is None:
        projection = ccrs.PlateCarree()
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1, projection=projection)
    lon2d, lat2d = np.meshgrid(data[longitude], data[latitude])

    if locator is None:
        cf = ax.contourf(lon2d, lat2d, data, levels=levels)
    else:
        cf = ax.contourf(lon2d, lat2d, data,
                         levels=levels,
                         locator=locator)

    fig.colorbar(cf)

    gl = ax.gridlines(crs=projection, draw_labels=True,
                      linewidth=1, color='gray', alpha=0.5, linestyle='--')
    gl.xlocator = mticker.AutoLocator()
    gl.ylocator = mticker.AutoLocator()
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    if filename is not None:
        fig.savefig(filename)

    if show is not None:
        plt.show()


def create_geographic_animation(dataset,
                                figsize=(10, 10),
                                projection=None,
                                longitude="longitude",
                                latitude="latitude",
                                levels=100,
                                locator=None,
                                interval=400
                                ):
    r"""
    Plots geographic data with a filled contourplot and creates an animation
    from the dataset.

    Parameters
    ----------
        dataset : `list(xarray.DataArray)`
            The dataset which should be plotted in a list.
        figsize : `tuple()`
            The size of the figure.
        projection : `Projection`
            A projection to be used for the geographic data. Default is
            `cartopy.crs.PlateCarree()`
        longitude : `str`
            Name of the longitude dimension in the dataset
        latitude : `str`
            Name of the latitude dimension in the dataset
        levels : `int`
            Number of levels to be used
        locator :
            If set it will be provided to matplotlib contourf
        interval : `int`:
            Milliseconds between the frames in the animation

    Returns
    -------
    `matplotlib.animation.FuncAnimation`
        Animation created by the matplotlib based on the given data and
        parameters.
    """
    # If projection is not set, use the default
    if projection is None:
        projection = ccrs.PlateCarree()
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1, projection=projection)

    def animate(i):
        lon2d, lat2d = np.meshgrid(dataset[i][longitude], dataset[i][latitude])
        if locator is None:
            cf = ax.contourf(lon2d, lat2d, dataset[i], levels=levels)
        else:
            cf = ax.contourf(lon2d, lat2d, dataset[i],
                             levels=levels,
                             locator=locator)

        gl = ax.gridlines(crs=projection, draw_labels=True,
                          linewidth=1, color='gray', alpha=0.5, linestyle='--')
        gl.xlocator = mticker.AutoLocator()
        gl.ylocator = mticker.AutoLocator()
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER

        return cf,
    anim = FuncAnimation(fig, animate,
                         frames=len(dataset), interval=400, blit=False)
    return anim
